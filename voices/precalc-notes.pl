#!/usr/bin/perl -w

use strict;

open F, "<../pitches.h";

my $cnt = 0;

while(<F>) {
	chomp;
	next unless /^#define\s+(NOTE_[A-GS0-9]+)\s+([0-9]+)/;
	print "#define P_$1 $cnt\n";
	$cnt++;
}
close F;
$cnt = 0;
open F, "<../pitches.h";
while(<F>) {
	chomp;
	next unless /^#define\s+(NOTE_[A-GS0-9]+)\s+([0-9]+)/;
	print "\t COMPILE_PRECALC_DDS($2), // $cnt=$1\n";
	$cnt++;
}
