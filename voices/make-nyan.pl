#!/usr/bin/perl -w

use strict;

open FIN, "<nyan2.voice1";

while(<FIN>) {
	chomp;
	next unless my ($duration, $note, $octave) = m/^([0-9]+)n*([A-GRn#]+)([0-9])?/;
	$octave += 1;
	if($note eq "R") {
		$note = "REST";
	} elsif($note =~ m/^##([A-G])/) {
		$note = "P_NOTE_".chr(ord($1)+1)."$octave";
	} elsif($note =~ m/^#([A-G])/) {
		$note = "P_NOTE_$1S$octave";
	} else {
		$note = "P_NOTE_$note$octave";
	}
	print "{ $note, $duration },\n";
}
