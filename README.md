This is an Arduino sketch to play the NyanCat theme using a wave shaped DDS PWM
output. It uses two voices to play the main melody and the bass line.

Post filtering is needed on Pin 3 to turn the PWM into smooth audio waves, which
is as simple as a 4.7k resistor and 10nF capacitor in an RC filter.