#include "DDS.h"
#include "pitches.h"
#include <avr/pgmspace.h>

const ddsAccumulator_t notePrecalc[P_NOTE_DS8+1] = {
	 COMPILE_PRECALC_DDS(0), // 0=NOTE_B0
	 COMPILE_PRECALC_DDS(33), // 1=NOTE_C1
	 COMPILE_PRECALC_DDS(35), // 2=NOTE_CS1
	 COMPILE_PRECALC_DDS(37), // 3=NOTE_D1
	 COMPILE_PRECALC_DDS(39), // 4=NOTE_DS1
	 COMPILE_PRECALC_DDS(41), // 5=NOTE_E1
	 COMPILE_PRECALC_DDS(44), // 6=NOTE_F1
	 COMPILE_PRECALC_DDS(46), // 7=NOTE_FS1
	 COMPILE_PRECALC_DDS(49), // 8=NOTE_G1
	 COMPILE_PRECALC_DDS(52), // 9=NOTE_GS1
	 COMPILE_PRECALC_DDS(55), // 10=NOTE_A1
	 COMPILE_PRECALC_DDS(58), // 11=NOTE_AS1
	 COMPILE_PRECALC_DDS(62), // 12=NOTE_B1
	 COMPILE_PRECALC_DDS(65), // 13=NOTE_C2
	 COMPILE_PRECALC_DDS(69), // 14=NOTE_CS2
	 COMPILE_PRECALC_DDS(73), // 15=NOTE_D2
	 COMPILE_PRECALC_DDS(78), // 16=NOTE_DS2
	 COMPILE_PRECALC_DDS(82), // 17=NOTE_E2
	 COMPILE_PRECALC_DDS(87), // 18=NOTE_F2
	 COMPILE_PRECALC_DDS(93), // 19=NOTE_FS2
	 COMPILE_PRECALC_DDS(98), // 20=NOTE_G2
	 COMPILE_PRECALC_DDS(104), // 21=NOTE_GS2
	 COMPILE_PRECALC_DDS(110), // 22=NOTE_A2
	 COMPILE_PRECALC_DDS(117), // 23=NOTE_AS2
	 COMPILE_PRECALC_DDS(123), // 24=NOTE_B2
	 COMPILE_PRECALC_DDS(131), // 25=NOTE_C3
	 COMPILE_PRECALC_DDS(139), // 26=NOTE_CS3
	 COMPILE_PRECALC_DDS(147), // 27=NOTE_D3
	 COMPILE_PRECALC_DDS(156), // 28=NOTE_DS3
	 COMPILE_PRECALC_DDS(165), // 29=NOTE_E3
	 COMPILE_PRECALC_DDS(175), // 30=NOTE_F3
	 COMPILE_PRECALC_DDS(185), // 31=NOTE_FS3
	 COMPILE_PRECALC_DDS(196), // 32=NOTE_G3
	 COMPILE_PRECALC_DDS(208), // 33=NOTE_GS3
	 COMPILE_PRECALC_DDS(220), // 34=NOTE_A3
	 COMPILE_PRECALC_DDS(233), // 35=NOTE_AS3
	 COMPILE_PRECALC_DDS(247), // 36=NOTE_B3
	 COMPILE_PRECALC_DDS(262), // 37=NOTE_C4
	 COMPILE_PRECALC_DDS(277), // 38=NOTE_CS4
	 COMPILE_PRECALC_DDS(294), // 39=NOTE_D4
	 COMPILE_PRECALC_DDS(311), // 40=NOTE_DS4
	 COMPILE_PRECALC_DDS(330), // 41=NOTE_E4
	 COMPILE_PRECALC_DDS(349), // 42=NOTE_F4
	 COMPILE_PRECALC_DDS(370), // 43=NOTE_FS4
	 COMPILE_PRECALC_DDS(392), // 44=NOTE_G4
	 COMPILE_PRECALC_DDS(415), // 45=NOTE_GS4
	 COMPILE_PRECALC_DDS(440), // 46=NOTE_A4
	 COMPILE_PRECALC_DDS(466), // 47=NOTE_AS4
	 COMPILE_PRECALC_DDS(494), // 48=NOTE_B4
	 COMPILE_PRECALC_DDS(523), // 49=NOTE_C5
	 COMPILE_PRECALC_DDS(554), // 50=NOTE_CS5
	 COMPILE_PRECALC_DDS(587), // 51=NOTE_D5
	 COMPILE_PRECALC_DDS(622), // 52=NOTE_DS5
	 COMPILE_PRECALC_DDS(659), // 53=NOTE_E5
	 COMPILE_PRECALC_DDS(698), // 54=NOTE_F5
	 COMPILE_PRECALC_DDS(740), // 55=NOTE_FS5
	 COMPILE_PRECALC_DDS(784), // 56=NOTE_G5
	 COMPILE_PRECALC_DDS(831), // 57=NOTE_GS5
	 COMPILE_PRECALC_DDS(880), // 58=NOTE_A5
	 COMPILE_PRECALC_DDS(932), // 59=NOTE_AS5
	 COMPILE_PRECALC_DDS(988), // 60=NOTE_B5
	 COMPILE_PRECALC_DDS(1047), // 61=NOTE_C6
	 COMPILE_PRECALC_DDS(1109), // 62=NOTE_CS6
	 COMPILE_PRECALC_DDS(1175), // 63=NOTE_D6
	 COMPILE_PRECALC_DDS(1245), // 64=NOTE_DS6
	 COMPILE_PRECALC_DDS(1319), // 65=NOTE_E6
	 COMPILE_PRECALC_DDS(1397), // 66=NOTE_F6
	 COMPILE_PRECALC_DDS(1480), // 67=NOTE_FS6
	 COMPILE_PRECALC_DDS(1568), // 68=NOTE_G6
	 COMPILE_PRECALC_DDS(1661), // 69=NOTE_GS6
	 COMPILE_PRECALC_DDS(1760), // 70=NOTE_A6
	 COMPILE_PRECALC_DDS(1865), // 71=NOTE_AS6
	 COMPILE_PRECALC_DDS(1976), // 72=NOTE_B6
	 COMPILE_PRECALC_DDS(2093), // 73=NOTE_C7
	 COMPILE_PRECALC_DDS(2217), // 74=NOTE_CS7
	 COMPILE_PRECALC_DDS(2349), // 75=NOTE_D7
	 COMPILE_PRECALC_DDS(2489), // 76=NOTE_DS7
	 COMPILE_PRECALC_DDS(2637), // 77=NOTE_E7
	 COMPILE_PRECALC_DDS(2794), // 78=NOTE_F7
	 COMPILE_PRECALC_DDS(2960), // 79=NOTE_FS7
	 COMPILE_PRECALC_DDS(3136), // 80=NOTE_G7
	 COMPILE_PRECALC_DDS(3322), // 81=NOTE_GS7
	 COMPILE_PRECALC_DDS(3520), // 82=NOTE_A7
	 COMPILE_PRECALC_DDS(3729), // 83=NOTE_AS7
	 COMPILE_PRECALC_DDS(3951), // 84=NOTE_B7
	 COMPILE_PRECALC_DDS(4186), // 85=NOTE_C8
	 COMPILE_PRECALC_DDS(4435), // 86=NOTE_CS8
	 COMPILE_PRECALC_DDS(4699), // 87=NOTE_D8
	 COMPILE_PRECALC_DDS(4978)  // 88=NOTE_DS8
};
