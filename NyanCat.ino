// Nyan Cat Awfulness
#define DDS_REFCLK_DEFAULT (250000/10)

#include <Adafruit_NeoPixel.h>
#include <CapacitiveSensor.h>

#include "pitches.h"
#include "DDS.h"

#define REST 0

#define BPM 142
// 0.88 of ticks
#define TIMING ((uint16_t)((120LL*DDS_REFCLK_DEFAULT)/(BPM)))
#define ATTACK 80
#define DECAY 80

CapacitiveSensor capsense = CapacitiveSensor(11,10);
Adafruit_NeoPixel pixels(8, 9, NEO_GRB + NEO_KHZ800);
volatile bool pixelCalc = true;

// For the looping, restart here
// old 31
static const uint8_t repeatStart[2] = {26,2};

struct noteState {
  volatile bool next;
  uint8_t current;
  volatile uint16_t duration;
  volatile uint16_t attack;
  const struct voice *voice;
  DDS *dds;
} note[2];

#include "voices.h"

DDS dds1, dds2;

inline uint16_t durToDelay(uint8_t dur) {
  switch(dur) {
    case 1:
      return (TIMING)<<1;
    case 4:
      return (TIMING/4)<<1;
    case 8:
      return (TIMING/8)<<1;
    case 16:
      return (TIMING/16)<<1;
    default:
      return (TIMING)<<1;
  }
}

void setup() {
  Serial.begin(115200);
  
  pinMode(3, OUTPUT);
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  
  // Initialize pixels
  pixels.begin();
  
  // Initialize note stucture
  note[0].next = true;
  note[1].next = true;
  note[0].voice = voice3;
  note[1].voice = voice4;
  note[0].dds = &dds1;
  note[1].dds = &dds2;

  pinMode(4, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  digitalWrite(4, LOW);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  dds1.setPhaseDeg(0);
  dds2.setPhaseDeg(45);
  dds1.start();
  dds1.on();
  dds1.setAmplitude(128);
  dds2.on();
  dds2.setAmplitude(128);
  //pinMode(11,INPUT);
}

volatile uint8_t loops=0;
volatile uint8_t dimmer = 255;

void loop() {
  uint8_t c;
  if(loops == 0) {
    digitalWrite(4, HIGH);
    dds1.off();
    dds2.off();
    pixels.clear();
    pixels.show();
    dimmer = 0;
    while(capsense.capacitiveSensor(30) < 120) {
    }
    cli();
    dimmer = 255;
    dds1.setPrecalcFrequency(0);
    dds2.setPrecalcFrequency(0);
    dds1.on();
    dds2.on();
    note[0].current = 0;
    note[1].current = 0;
    loops = 3;
    digitalWrite(4, LOW);
    sei();
  } else {
    if(pixelCalc) {
      for(c = 0; c<2; c++) {
        pixels.setPixelColor(c, pixels.Color(note[c].voice[note[c].current].duration<<4,note[c].voice[note[c].current].note<<3,note[c].current));
        pixels.setPixelColor(c+2, pixels.Color(note[c].current, note[c].voice[note[c].current].duration<<4, note[c].voice[note[c].current].note<<3));
        pixels.setPixelColor(c+4, pixels.Color(note[c].voice[note[c].current].note<<3,note[c].current,note[c].voice[note[c].current].duration<<4));
        pixels.setPixelColor(c+6, pixels.Color(note[c].voice[note[c].current].duration<<4,note[c].voice[note[c].current].note<<3,note[c].current));
      }
      //pixels.setBrightness(64);
      pixels.show();
      pixelCalc = false;
    }
  }
}

ISR(ADC_vect) {
  static uint8_t c;
  static uint8_t nextOCR = 0;
  TIFR1 = _BV(ICF1);
  if(loops == 0) {
    return;
  }
  OCR2B = nextOCR;
  //PORTD ^= _BV(7);
  for(c = 0; c < 2; c++) {
    uint16_t newAmpl;
    if(note[c].duration < DECAY && note[c].duration>0) {
      newAmpl = ((note[c].duration >> (0))*dimmer);
      note[c].dds->setAmplitude(newAmpl>>8);
    } else if(++note[c].attack < ATTACK) {
      newAmpl = ((note[c].attack >> (0))*dimmer);
      note[c].dds->setAmplitude(newAmpl>>8);
    }
    note[c].dds->advanceTick();
    if(!note[c].next && --note[c].duration == 0)
      note[c].next = true;
  }
  
  nextOCR = (dds1.getDutyCycle() + dds2.getDutyCycle())>>1;
  
  for(c = 0; c < 2; c++) {
    if(note[c].next) {
      note[c].duration = durToDelay(note[c].voice[note[c].current].duration);
      note[c].dds->setPrecalcFrequency(notePrecalc[note[c].voice[note[c].current].note]);
      note[c].attack = 0;
      note[c].next = false;
      if(c == 0 && loops == 1 && note[0].current >= 230) {
        dimmer >>= 1;
      }
      if(note[c].voice[++note[c].current].duration == 0) {
        note[c].current = repeatStart[c];
        if(c == 0)
          --loops;
      }
      pixelCalc = true;
    }
  }
}
